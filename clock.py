class Clock:

    # instance attribute
    def __init__(self, hour: int, minute: int):
        one_hour_in_min = 60
        hour_in_a_day = 24

        print("Clock is rdy")
        self.total_minutes = (hour * one_hour_in_min+ minute) % (one_hour_in_min * hour_in_a_day)  # 60 minutes x 24hours = 1jour
        self.minute = str(self.total_minutes % one_hour_in_min)
        self.hour = str(self.total_minutes // one_hour_in_min)


    #Transformation en string de l'heure
    def __repr__(self):
        hour = self.hour.zfill(2)
        minute = self.minute.zfill(2)
        return f'{hour}:{minute}'

    #Addition
    def __add__(self, minutes):
        return Clock(0, self.total_minutes + minutes)

    #Soustraction
    def __sub__(self, minutes):
        return Clock(0,self.total_minutes - minutes)

    #Verif égalité
    def __eq__(self, other):
        return self.total_minutes == other.total_minutes



