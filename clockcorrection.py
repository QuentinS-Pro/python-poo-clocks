class Clock:
    '''
    Cette class permet de pouvoir faire les conversions necessaire pour pouvoir avoir l'heure
    '''
    def __init__(self, hour: int, minute: int):
        '''
        :param hour: nombre d'heure
        :param minute: nombre de minutes

        one_hour et nb_hour_in_day sont des constantes utilisées pour
        les conversions en minutes pour simplifier le code.
        '''
        one_hour = 60
        nb_hour_in_day = 24

        self.total_minutes = (hour * one_hour + minute) % (one_hour * nb_hour_in_day)
        self.hour = str(self.total_minutes // one_hour)
        self.minute = str(self.total_minutes % one_hour)

    def __repr__(self):
        '''
        sert pour le format d'affichage en rajoutant un "0"
        pour l'affichage de lheure si besoin
        :return:
        '''
        hour = self.hour.zfill(2)
        minute = self.minute.zfill(2)
        return f"{hour}:{minute}"

    def __eq__(self, other):
        '''
        traite les equivalences
        '''
        return self.total_minutes == other.total_minutes

    def __add__(self, minutes):
        '''
        ajoute les minutes
        '''
        return Clock(0, self.total_minutes + minutes)

    def __sub__(self, minutes):
        '''
        soustrait les minutes
        '''
        return Clock(0, self.total_minutes - minutes)